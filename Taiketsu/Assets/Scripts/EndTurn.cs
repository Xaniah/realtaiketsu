﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;




public class EndTurn : MonoBehaviour
{

    public List<Image> enemyCards = new List<Image>();
    public List<Image> yourCards = new List<Image>();

    public struct Card
    {
        public int Strength;
        public int Agility;
        public int Intelligence;
        public int Charisma;
    }

    Card yourCard = new Card();
    Card enemycard = new Card();

    public int turnNumber;
    int yourCardsLeft = 5;
    int enemyCardsleft = 5;
    int drawCard;


    public void opponentTurn()
    {
        if (turnNumber == 0)
        {
            yourCard.Strength = 750;
            yourCard.Agility = 400;
            yourCard.Intelligence = 570;
            yourCard.Charisma = 610;

            enemycard.Strength = 700;
            enemycard.Agility = 370;
            enemycard.Intelligence = 520;
            enemycard.Charisma = 580;
            
            Fight();
        }

        else if (turnNumber == 1)
        {
            yourCard.Strength = 310;
            yourCard.Agility = 630;
            yourCard.Intelligence = 440;
            yourCard.Charisma = 720;

            enemycard.Strength = 530;
            enemycard.Agility = 510;
            enemycard.Intelligence = 460;
            enemycard.Charisma = 610;

            Fight();
        }
        else if (turnNumber == 2)
        {
            yourCard.Strength = 330;
            yourCard.Agility = 530;
            yourCard.Intelligence = 810;
            yourCard.Charisma = 150;

            enemycard.Strength = 420;
            enemycard.Agility = 710;
            enemycard.Intelligence = 730;
            enemycard.Charisma = 690;

            Fight();
        }
        else if (turnNumber == 3)
        {
            yourCard.Strength = 430;
            yourCard.Agility = 230;
            yourCard.Intelligence = 710;
            yourCard.Charisma = 950;

            enemycard.Strength = 220;
            enemycard.Agility = 210;
            enemycard.Intelligence = 230;
            enemycard.Charisma = 290;

            Fight();
        }
        else if (turnNumber == 4)
        {
            yourCard.Strength = 430;
            yourCard.Agility = 630;
            yourCard.Intelligence = 210;
            yourCard.Charisma = 550;

            enemycard.Strength = 120;
            enemycard.Agility = 110;
            enemycard.Intelligence = 130;
            enemycard.Charisma = 190;

            Fight();
        }
        if (yourCardsLeft <= 0 || enemyCardsleft <= 0)
        {
            SceneManager.LoadScene("Name", LoadSceneMode.Single);
        }
    }

    void Fight()
    {
        drawCard = turnNumber + 3;

        //Checking for the winning card. Destroying the losing card and putting the winning card in the deck.
        //Drawing cards for both teams.
        if (Drag.statStrength == true && yourCard.Strength > enemycard.Strength)
        {
            Destroy(enemyCards[turnNumber]);
            yourCards[turnNumber].transform.SetParent(Drag.cardHand);
            yourCards[turnNumber].gameObject.SetActive(false);
            enemyCards[turnNumber].gameObject.SetActive(false);

            if (drawCard <= 4)
            {
                enemyCards[drawCard].gameObject.SetActive(true);
                yourCards[drawCard].gameObject.SetActive(true);
            }
            enemyCardsleft -= 1;
        }
        else if (Drag.statAgility == true && yourCard.Agility > enemycard.Agility)
        {
            Destroy(enemyCards[turnNumber]);
            yourCards[turnNumber].transform.SetParent(Drag.cardHand);
            yourCards[turnNumber].gameObject.SetActive(false);
            enemyCards[turnNumber].gameObject.SetActive(false);

            if (drawCard <= 4)
            {
                enemyCards[turnNumber + 3].gameObject.SetActive(true);
                yourCards[turnNumber + 3].gameObject.SetActive(true);
            }
            enemyCardsleft -= 1;
        }
        else if (Drag.statIntelligence == true && yourCard.Intelligence > enemycard.Intelligence)
        {
            Destroy(enemyCards[turnNumber]);
            yourCards[turnNumber].transform.SetParent(Drag.cardHand);
            yourCards[turnNumber].gameObject.SetActive(false);
            enemyCards[turnNumber].gameObject.SetActive(false);

            if (drawCard <= 4)
            {
                enemyCards[turnNumber + 3].gameObject.SetActive(true);
                yourCards[turnNumber + 3].gameObject.SetActive(true);
            }
            enemyCardsleft -= 1;
        }
        else if (Drag.statCharsima == true && yourCard.Charisma > enemycard.Charisma)
        {
            Destroy(enemyCards[turnNumber]);
            yourCards[turnNumber].transform.SetParent(Drag.cardHand);
            yourCards[turnNumber].gameObject.SetActive(false);
            enemyCards[turnNumber].gameObject.SetActive(false);

            if (drawCard <= 4)
            {
                enemyCards[turnNumber + 3].gameObject.SetActive(true);
                yourCards[turnNumber + 3].gameObject.SetActive(true);
            }
            enemyCardsleft -= 1;
        }
        else if (Drag.statStrength == true && yourCard.Strength < enemycard.Strength)
        {
            Destroy(yourCards[turnNumber]);
            enemyCards[turnNumber].transform.SetParent(Drag.opponenCardHand);
            enemyCards[turnNumber].gameObject.SetActive(false);
            yourCards[turnNumber].gameObject.SetActive(false);

            if (drawCard <= 4)
            {
                enemyCards[turnNumber + 3].gameObject.SetActive(true);
                yourCards[turnNumber + 3].gameObject.SetActive(true);
            }
            yourCardsLeft -= 1;
        }
        else if (Drag.statAgility == true && yourCard.Agility < enemycard.Agility)
        {
            Destroy(yourCards[turnNumber]);
            enemyCards[turnNumber].transform.SetParent(Drag.opponenCardHand);
            enemyCards[turnNumber].gameObject.SetActive(false);
            yourCards[turnNumber].gameObject.SetActive(false);

            if (drawCard <= 4)
            {
                enemyCards[turnNumber + 3].gameObject.SetActive(true);
                yourCards[turnNumber + 3].gameObject.SetActive(true);
            }
            yourCardsLeft -= 1;
        }
        else if (Drag.statIntelligence == true && yourCard.Intelligence < enemycard.Intelligence)
        {
            Destroy(yourCards[turnNumber]);
            enemyCards[turnNumber].transform.SetParent(Drag.opponenCardHand);
            enemyCards[turnNumber].gameObject.SetActive(false);
            yourCards[turnNumber].gameObject.SetActive(false);

            if (drawCard <= 4)
            {
                enemyCards[turnNumber + 3].gameObject.SetActive(true);
                yourCards[turnNumber + 3].gameObject.SetActive(true);
            }
            yourCardsLeft -= 1;
        }
        else if (Drag.statCharsima == true && yourCard.Charisma < enemycard.Charisma)
        {
            Destroy(yourCards[turnNumber]);
            enemyCards[turnNumber].transform.SetParent(Drag.opponenCardHand);
            enemyCards[turnNumber].gameObject.SetActive(false);
            yourCards[turnNumber].gameObject.SetActive(false);

            if (drawCard <= 4)
            {
                enemyCards[turnNumber + 3].gameObject.SetActive(true);
                yourCards[turnNumber + 3].gameObject.SetActive(true);
            }
            yourCardsLeft -= 1;
        }
        turnNumber += 1;
        Drag.droppedCards = 0;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Drag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    Transform boardTransform;
    public static Transform cardHand;
    public static Transform opponenCardHand;
    public Transform parentToReturnTo = null;
    public static int droppedCards = 0;
    public GameObject chooseStat;
    public Button Endturn;

    public static bool statStrength = false;
    public static bool statAgility = false;
    public static bool statIntelligence = false;
    public static bool statCharsima = false;

    EndTurn turnEnd;


    void Start()
    {
        boardTransform = GameObject.FindGameObjectWithTag("Board").transform;
        cardHand = GameObject.FindGameObjectWithTag("Hand").transform;
        opponenCardHand = GameObject.FindGameObjectWithTag("opponentHand").transform;
        turnEnd = gameObject.GetComponent<EndTurn>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (droppedCards == 0)
        {
            //Setting the parent to return to, to its current parent
            parentToReturnTo = this.transform.parent;
            //Setting the curent parent to the parent of its parent
            this.transform.SetParent(this.transform.parent.parent);

            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
             
    }
    
    public void OnDrag(PointerEventData eventData)
    {
        if (droppedCards == 0 /*&& turnEnd.yourCards[turnEnd.turnNumber - 1]*/)
        {
            //Dragging the card with the mouse
            this.transform.position = eventData.position;
        } 
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //Setting the parent back to its parent to return to
        this.transform.SetParent(parentToReturnTo);
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        

        if (transform.parent == boardTransform && droppedCards == 0) 
        {
            //Setting the amount of dropped cards to 1
            droppedCards = 1;
            GetComponent<Drag>().enabled = false;
            chooseStat.SetActive(true);
            //Endturn.enabled = false;
        }
    }

    public void strength()
    {
        statStrength = true;
        chooseStat.SetActive(false);
        //turnEnd.yourCards[turnEnd.turnNumber + 2].enabled = true;
        //turnEnd.yourCards[turnEnd.turnNumber - 1].GetComponent<Drag>().enabled = true;
        //Endturn.enabled = true;
    }
    public void agility()
    {
        statAgility = true;
        chooseStat.SetActive(false);
        //turnEnd.yourCards[turnEnd.turnNumber + 2].enabled = true;
        //turnEnd.yourCards[turnEnd.turnNumber - 1].GetComponent<Drag>().enabled = true;
        //Endturn.enabled = true;
    }
    public void intelligence()
    {
        statIntelligence = true;
        chooseStat.SetActive(false);
        //turnEnd.yourCards[turnEnd.turnNumber + 2].enabled = true;
        //turnEnd.yourCards[turnEnd.turnNumber - 1].GetComponent<Drag>().enabled = true;
        //Endturn.enabled = true;
    }
    public void charisma()
    {
        statCharsima = true;
        chooseStat.SetActive(false);
        //turnEnd.yourCards[turnEnd.turnNumber + 2].enabled = true;
        //turnEnd.yourCards[turnEnd.turnNumber - 1].GetComponent<Drag>().enabled = true;
        //Endturn.enabled = true;
    }
}
